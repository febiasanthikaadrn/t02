package com.company;

public class LCD {

    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;

    private int CableOption = 1;

    public String turnOn() {
        return Status = "On";
    }

    public String turnOff() {
        return Status = "Off";
    }

    public String Freeze() {
        return Status = "Freeze";
    }

    public int VolumeUp() {
        return Volume++;
    }

    public int VolumeDown() {
        return Volume--;
    }

    public void setVolume(int Volume) {
        this.Volume = Volume;
    }

    public int BrightnessUp() {
        return Brightness++;
    }

    public int BrightnessDown() {
        return Brightness--;
    }

    public void setBrightness(int Brightness) {
        this.Brightness = Brightness;
    }

    public String CableUp() {
        return Cable;
    }

    public String CableDown() {
        return Cable;
    }

    public void cableLCD() {
        switch (CableOption) {
            case 1 :
                Cable = "DVI";
                break;
            case 2 :
                Cable = "VGA";
                break;
            case 3 :
                Cable = "HDMI";
                break;
            default:
                Cable = "Pilihan cable tidak tersedia";
                break;
        }
    }

    public void setCable() {
        this.Cable = Cable;
    }

    public void displayMessage() {
        System.out.println("-------------- LCD ---------------");
        System.out.println("Status LCD saat ini     : " + Status);
        System.out.println("Volume LCD saat ini     : " + Volume);
        System.out.println("Brightness LCD saat ini : " + Brightness);
        System.out.println("Cable yang digunakan    : " + Cable );
    }

}
