package com.company;

public class Main {

    public static void main(String[] args) {

        LCD lcd = new LCD();
        lcd.turnOff();
        lcd.turnOn();
        lcd.Freeze();
        lcd.setVolume(50);
        lcd.VolumeUp();
        lcd.setBrightness(39);
        lcd.BrightnessDown();
        lcd.cableLCD();

        lcd.displayMessage();
    }
}
